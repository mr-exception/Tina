package tina;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class main {
	public static void main(String[] args) throws FileNotFoundException {
		DocumentManager docMan = new DocumentManager("tina", "root", "", 2);

		File docsDir = new File("docs");
		File[] docList = docsDir.listFiles();
		for (int i = 0; i < docList.length; i++) {
			String title = docList[i].getName().split("[.]")[0];
			String content = readAllData(docList[i].getPath());
			System.out.println("reading: " + title);
			docMan.addDocument(title, content);
		}
		docMan.startLevelA();
	}

	private static String readAllData(String path) {
		String result = "";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(path));
			char[] buf = new char[1024 * 4];
			int len;
			while ((len = reader.read(buf)) > 0) {
				result += String.valueOf(Arrays.copyOfRange(buf, 0, len));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
