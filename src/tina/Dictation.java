package tina;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

/**
 * this class correct the dictations of a document
 * @author mr-exception
 */
public class Dictation {
	public int minDist(String a, String b) {
		int aLen = a.length();
		int bLen = b.length();
		int[][] DistArr = new int[aLen][bLen];
		for (int i = 0; i < aLen; i++)
			DistArr[i][0] = i;
		for (int i = 0; i < bLen; i++)
			DistArr[0][i] = i;
		for (int i = 1; i < aLen; i++)
			for (int j = 1; j < bLen; j++) {
				int minDist = DistArr[i - 1][j] + 1;
				int tmp = DistArr[i][j - 1] + 1;
				if (minDist > tmp)
					minDist = tmp;
				tmp = DistArr[i - 1][j - 1];
				if (a.charAt(i) != b.charAt(j))
					tmp += 2;
				if (minDist > tmp)
					minDist = tmp;
				DistArr[i][j] = minDist;
			}
		return DistArr[aLen - 1][bLen - 1];
	}
/**
 * find the best form of a word in an array of words
 * @param word
 * @param wordList
 * @return the best word from the array given
 */
	public String findBestFrom(String word, ArrayList<String> wordList) {
		String result = null;
		int minDist = word.length() * 4;
		for (String cWord : wordList) {
			int t = minDist(word, cWord);
			if (t < minDist) {
				minDist = t;
				result = cWord;
			}
		}
		return result;
	}

	public ArrayList<Character> findWhiteSpace(String content, int r) {
		ArrayList<charRepeat> charList = new ArrayList<charRepeat>();

		for (int i = 0; i < content.length(); i++) {
			charRepeat ch = new charRepeat(content.charAt(i));
			if (charList.contains(ch)) {
				int pos = charList.indexOf(ch);
				charList.get(pos).r++;
			} else {
				charList.add(ch);
			}
		}
		charList.sort(new Comparator<charRepeat>() {

			@Override
			public int compare(charRepeat arg0, charRepeat arg1) {
				if (arg0.r > arg1.r)
					return -1;
				else if (arg0.r < arg1.r)
					return 1;
				else
					return 0;
			}
		});

		ArrayList<Character> result = new ArrayList<Character>();
		for (int i = 0; i < r; i++)
			result.add(charList.get(i).c);
		return result;
	}

	private class charRepeat {
		char c;
		int r;

		public charRepeat(char c) {
			this.c = c;
			this.r = 1;
		}

		@Override
		public boolean equals(Object arg0) {
			charRepeat chr = (charRepeat) arg0;
			return chr.c == this.c;
		}
	}
}