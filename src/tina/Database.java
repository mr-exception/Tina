package tina;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

public class Database {
	private Connection conn;
	private Statement stmt;

	public Database(String dbName, String dbUsername, String dbPass) {
		String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		String dbUrl = "jdbc:mysql://localhost/" + dbName + "?allowMultiQueries=true";
		try {
			Class.forName(JDBC_DRIVER);
			System.out.print("Connecting to mysql database...");
			conn = DriverManager.getConnection(dbUrl, dbUsername, dbPass);
			System.out.println("\rConnected to mysql database;");
			System.out.print("Creating mysql jdbc statement...");
			stmt = conn.createStatement();
			System.out.println("\rstatement msql jdbc created;");
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("\rError: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public boolean add_stop_word(String word) {
		ResultSet word_row = get_stop_word(word);
		if (word_row == null) {
			try {
				stmt.execute("INSERT INTO `stopword` (`word`) VALUES ('" + word + "')");

			} catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		} else
			return false;
	}

	public ResultSet get_stop_word(String word) {
		ResultSet result = null;
		try {
			result = stmt.executeQuery("SELECT * FROM `stopword` WHERE `word` = '" + word + "'");
			if (!result.next())
				return null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public ResultSet get_class(String doc_Id) {
		ResultSet result = null;
		try {
			result = stmt.executeQuery("SELECT * FROM `doc` WHERE `doc_Id` = " + doc_Id);
			if (!result.next())
				return null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean add_class(String doc_title) {
		try {
			stmt.execute("INSERT INTO `doc` (`title`) VALUES ('" + doc_title + "')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean add_new_word(String word, int doc_Id) {
		ResultSet word_row = get_word(word, doc_Id);
		if (word_row == null) {
			try {
				stmt.execute(
						"INSERT INTO `words`(`word`, `doc_Id`, `repeat`) VALUES ('" + word + "','" + doc_Id + "',1)");
				stmt.execute("UPDATE `doc` SET `size`= `size` + 1 , `word_count` = `word_count` + 1 WHERE `doc_Id` = "
						+ doc_Id);

			} catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		} else {
			try {
				stmt.execute("UPDATE `words` SET `repeat`= `repeat` + 1 WHERE `word` = '" + word + "' AND `doc_Id` = "
						+ doc_Id);
				stmt.execute("UPDATE `doc` SET `size` = `size` + 1 WHERE `doc_Id` = " + doc_Id);

			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public boolean add_raw_word(String word, String doc_title) {
		try {
			stmt.execute("INSERT INTO `raw_word`(`word`, `doc_Id`) VALUES ('" + word + "','" + doc_title + "')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean create_n_table(int n) {
		try {
			stmt.execute(" CREATE TABLE IF NOT EXISTS `word_" + n
					+ "` ( `word` varchar(100) NOT NULL, `doc` varchar(100) NOT NULL )");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean add_n_gram(String word, String document_title, int n) {
		try {
			stmt.execute(
					"INSERT INTO `word_" + n + "`(`word`, `doc`) VALUES ('" + word + "','" + document_title + "')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public ResultSet get_word(String word, int doc_Id) {
		ResultSet result = null;
		try {
			result = stmt.executeQuery("SELECT * FROM `words` WHERE `word` = '" + word + "' AND `doc_Id` = " + doc_Id);
			if (!result.next())
				return null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public double get_word_in_class(String word, int doc_Id) {
		ResultSet result = null;
		try {
			System.out.println("SELECT * FROM `words` WHERE `word` = '" + word + "' AND `doc_Id` = " + doc_Id);
			result = stmt.executeQuery("SELECT * FROM `words` WHERE `word` = '" + word + "' AND `doc_Id` = " + doc_Id);
			if (result.next()) {
				return result.getLong("repeat");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double get_count_c(int doc_Id) {
		ResultSet result = null;
		try {
			result = stmt.executeQuery("SELECT * FROM `doc` WHERE `doc_Id` = " + doc_Id);
			if (result.next()) {
				return result.getLong("size");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;

	}

	public double get_count_v() {
		ResultSet result = null;
		double res = 0;
		try {
			result = stmt.executeQuery("SELECT * FROM `doc` WHERE 1");
			while (result.next())
				res += result.getLong("size");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public ResultSet get_category_list() {
		ResultSet result = null;
		try {
			result = stmt.executeQuery("SELECT * FROM `doc` WHERE 1;");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public int get_doc_size(int doc_Id) {
		ResultSet result = null;
		try {
			result = stmt.executeQuery("SELECT * FROM `doc` WHERE `doc_Id` = " + doc_Id);
			if (result.next())
				return result.getInt("size");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String get_doc_title(int doc_Id) {
		ResultSet result = null;
		try {
			result = stmt.executeQuery("SELECT * FROM `doc` WHERE `doc_Id` = " + doc_Id);
			if (result.next())
				return result.getString("title");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean reset() {
		try {
			System.out.println("reseting data base");
			stmt.execute("DELETE FROM `doc` WHERE 1");
			System.out.println("\tcategories reseted");
			stmt.execute("DELETE FROM `words` WHERE 1");
			stmt.execute("DELETE FROM `raw_word` WHERE 1");
			System.out.println("\tword list reseted");
			stmt.execute("DELETE FROM `stopword` WHERE 1");
			System.out.println("\tstopwordlist reseted");
			System.out.println("all data removed from data base");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
