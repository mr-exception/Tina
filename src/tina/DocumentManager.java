package tina;

import java.util.ArrayList;

public class DocumentManager {
	ArrayList<Document> DocList = new ArrayList<Document>();

	private Database db;

	/*
	 * configs: here are some vars about configs
	 */
	private float totalRatio;
	private float totalhighRepeat;

	/**
	 * manage documents, add, remove, edit and injection about documents, this
	 * is an interface between application and databse
	 * 
	 * @param dbName
	 * @param dbUsername
	 * @param dbPass
	 * @param maxRatio
	 */
	public DocumentManager(String dbName, String dbUsername, String dbPass, int maxRatio) {
		db = new Database(dbName, dbUsername, dbPass);
	}

	/**
	 * add a new docuemtn to the data
	 * 
	 * @param title
	 *            document title or it's class
	 * @param content
	 */
	public void addDocument(String title, String content) {
		Document doc = new Document(content, title);
		this.DocList.add(doc);
		totalRatio += doc.getRatio();
		totalhighRepeat += doc.getMaxReapet();
	}

	/**
	 * add a new document to the temp data
	 * 
	 * @param doc
	 *            document object
	 */
	public void addDocument(Document doc) {
		this.DocList.add(doc);
		totalRatio += doc.getRatio();
	}

	/**
	 * after a while adding temp documents, all documents would be normalized
	 * and added into database, then there is no docuemtn in buffer
	 */
	void startLevelA() {
		float avgRatio = (totalRatio / DocList.size());
		float avgTop = (totalhighRepeat / DocList.size());

		float avgBorder = (avgRatio * 2 + avgTop) / 3;

		System.out.println("level A started for density " + avgRatio);
		for (int i = 0; i < DocList.size(); i++) {
			ArrayList<String> wList = DocList.get(i).getWordsReapetMoreThan(avgBorder);
			System.out.println("------------ " + DocList.get(i).getTitle());
			for (String word : wList) {
				System.out.println(word);
			}
		}
	}
}
