package tina;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.BiConsumer;

public class Document {
	private String content;
	private String title;
	HashMap<String, Integer> wordMap;

	private Dictation myDicClass;

	private String splitChar;

	private int wordCount;
	private int keyCount;
	private int maxReapet;

	public int getWordCount() {
		return wordCount;
	}

	public int getKeyCount() {
		return keyCount;
	}

	/**
	 * holds a document information
	 * 
	 * @param content
	 * @param title
	 */
	public Document(String content, String title) {
		this.content = content;
		this.title = title;
		this.myDicClass = new Dictation();
		wordMap = new HashMap<String, Integer>();
		findSplitChar();
		setUpWords();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	private void setUpWords() {
		String[] wordsArray = content.split(this.splitChar);
		for (String word : wordsArray) {
			if (wordMap.containsKey(word)) {
				int count = wordMap.get(word);
				count++;
				wordMap.put(word, count);
				wordCount++;

				if (count > maxReapet)
					maxReapet = count;
			} else {
				wordMap.put(word, 1);
				wordCount++;
				keyCount++;
			}
		}
		// this.printWordList();
		System.out.println("ratio: " + getRatio());
	}

	public int getMaxReapet() {
		return maxReapet;
	}

	private void findSplitChar() {
		this.splitChar = String.valueOf(myDicClass.findWhiteSpace(this.content, 1).get(0));
	}

	public void printWordList() {
		this.wordMap.forEach(new BiConsumer<String, Integer>() {

			@Override
			public void accept(String arg0, Integer arg1) {
				System.out.println(title + " -> " + arg0 + " : " + arg1);
			}
		});
		System.out.println();
	}

	public float getRatio() {
		return (float) wordCount / (float) keyCount;
	}

	public float getperCentInClass(String word) {
		return wordMap.get(word) / wordCount;
	}

	public ArrayList<String> getWordsReapetMoreThan(final float m) {
		final ArrayList<String> result = new ArrayList<String>();
		wordMap.forEach(new BiConsumer<String, Integer>() {
			@Override
			public void accept(String arg0, Integer arg1) {
				if (arg1 > m)
					result.add(arg0);
			}
		});
		return result;
	}
}
